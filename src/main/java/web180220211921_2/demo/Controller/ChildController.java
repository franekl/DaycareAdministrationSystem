package web180220211921_2.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import web180220211921_2.demo.Model.Child;
import web180220211921_2.demo.Service.IChildService;

import java.util.List;

@Controller
public class ChildController {
    @Autowired
    IChildService ChildService;

    @GetMapping("child/index")
    public String Home(Model model){
        List<Child> childList = ChildService.fetchActive();
        model.addAttribute("children",childList);
        return "child/index";
    }

    @GetMapping("child/pending")
    public String pending(Model model){
        List<Child> childList = ChildService.fetchWaitingList();
        model.addAttribute("children",childList);
        return "child/pending";
    }

    @GetMapping("child/accept/{id}")
    public String accept(@PathVariable("id") int id){
        boolean b = ChildService.acceptChild(id);
        if(b)
        return "redirect:http://localhost:5050/child/index";
        else
            return "home/error";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping("child/create/")
    public String create(@ModelAttribute Child child){
        //Hardcoded because of mystery errors
        if(child.getParentId() == 0)
            child.setParentId(3);
        ChildService.addChild(child);
        return "redirect:http://localhost:5050/child/index";
    }


    @GetMapping("child/create")
    public String create(){
        return "child/create";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping("child/update/{id}")
    public String update(@PathVariable("id") int id, @ModelAttribute Child child){
        //Hardcoded because of mystery errors
        if(child.getParentId() == 0){
            child.setParentId(3);
        }
        ChildService.updateChild(id, child);
        return "redirect:http://localhost:5050/child/index";
    }


    @GetMapping("child/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("child",ChildService.findChildById(id));
        return "child/update";
    }

    @GetMapping("child/delete/{id}")
    public String delete(@PathVariable("id") int id){
        Boolean delete = ChildService.deleteChild(id);
        if(delete)
            return "redirect:http://localhost:5050/child/index";
        else
            return "home/error";
    }
}
