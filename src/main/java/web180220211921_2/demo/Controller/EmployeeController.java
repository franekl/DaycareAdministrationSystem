package web180220211921_2.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web180220211921_2.demo.Model.Employee;
import web180220211921_2.demo.Service.IEmployeeService;

import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    IEmployeeService employeeService;

    @GetMapping("employee/index")
    public String Home(Model model){
        List<Employee> employeeList = employeeService.fetchAll();
        model.addAttribute("employees",employeeList);
        return "employee/index";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping(value="employee/create")
    public String create(@ModelAttribute Employee employee){
        employeeService.addEmployee(employee);
        return "redirect:http://localhost:5050/employee/index";
    }

    // Called by the form tag and here we use metode=post
    @GetMapping(value="employee/create")
    public String create(){
        return "employee/create";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping("employee/update/{id}")
    public String update(@PathVariable("id") int id, @ModelAttribute Employee employee){
        employeeService.updateEmployee(id, employee);
        return "redirect:http://localhost:5050/employee/index";
    }

    @GetMapping("employee/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("employee",employeeService.findEmployeeById(id));
        return "employee/update";
    }

    @GetMapping("employee/delete/{id}")
    public String delete(@PathVariable("id") int id){
        Boolean delete = employeeService.deleteEmployee(id);
        if(delete)
            return "redirect:http://localhost:5050/employee/index";
        else
            return "home/error";
    }

    @PostMapping("employee/signin")
    public String signin(@ModelAttribute Employee employee){
        try{
            if(employeeService.signin(employee.getId(), employee.getPin())){
                return "redirect:http://localhost:5050/home";
            }
            else
                return "home/error";
        }
        catch (Exception e){
            return "home/error";
        }
    }
}
