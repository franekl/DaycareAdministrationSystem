package web180220211921_2.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String Index(){
        return "home/index";
    }

    @GetMapping("/home")
    public String Home(){
        return "home/home";
    }
}
