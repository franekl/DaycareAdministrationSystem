package web180220211921_2.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import web180220211921_2.demo.Model.Parent;
import web180220211921_2.demo.Service.IParentService;
import java.util.List;

@Controller
public class ParentController {
    @Autowired
    IParentService parentService;

    @GetMapping("parent/index")
    public String Home(Model model){
        List<Parent> parentList = parentService.fetchAll();
        model.addAttribute("parents",parentList);
        return "parent/index";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping("parent/create")
    public String create(@ModelAttribute Parent parent){
        parentService.addParent(parent);
        return "redirect:http://localhost:5050/parent/index";
    }


    @GetMapping("parent/create")
    public String create(){
        return "parent/create";
    }

    // Called by the form tag and here we use metode=post
    @PostMapping("parent/update/{id}")
    public String update(@PathVariable("id") int id, @ModelAttribute Parent parent){
        parent.setChildren_no(parentService.findParentById(id).getChildren_no());

        parentService.updateParent(id, parent);
        return "redirect:http://localhost:5050/parent/index";
    }


    @GetMapping("parent/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("parent",parentService.findParentById(id));
        return "parent/update";
    }

    @GetMapping("parent/delete/{id}")
    public String delete(@PathVariable("id") int id){
        Boolean delete = parentService.deleteParent(id);
        if(delete)
            return "redirect:http://localhost:5050/parent/index";
        else
            return "home/error";
    }
}
