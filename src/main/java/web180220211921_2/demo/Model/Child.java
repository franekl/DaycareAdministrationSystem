package web180220211921_2.demo.Model;

import org.springframework.data.annotation.Id;
import javax.persistence.Entity;

@Entity
public class Child {
    @Id
    private int id;

    private int parent_id;
    private String first_name;
    private String last_name;
    private int age;
    private boolean is_active;

    public Child(int id,int parent_id, String first_name, String last_name, int age, boolean is_active) {
        this.id = id;
        this.parent_id = parent_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.is_active = is_active;
    }

    public Child() {

    }
    @javax.persistence.Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parent_id;
    }

    public void setParentId(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }
}
