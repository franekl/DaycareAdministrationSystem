package web180220211921_2.demo.Model;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;

@Entity
public class Employee {
    @Id
    private int id;

    private String pin;
    private String first_name;
    private String last_name;
    private String address;
    private String phone_no;
    private String work_days;

    public Employee(){}

    public Employee(int id, String pin, String first_name, String last_name, String address, String phone_no, String work_days) {
        this.id = id;
        this.pin=pin;
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.phone_no = phone_no;
        this.work_days = work_days;
    }
    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String firstName) {
        this.first_name = firstName;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String lastName) {
        this.last_name = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phoneNumber) {
        this.phone_no = phoneNumber;
    }

    public String getWork_days() {
        return work_days;
    }

    public void setWork_days(String workDays) {
        this.work_days = workDays;
    }


    public void setId(int id) {
        this.id = id;
    }
    @javax.persistence.Id
    public int getId() {
        return id;
    }
}
