package web180220211921_2.demo.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Parent {
    @Id
    private int id;

    private String first_name;
    private String last_name;
    private String address;
    private String phone_no;

    private int children_no;

    public Parent(int id, String first_name, String last_name, String address, String phone_no, ArrayList<Child> children, int children_no) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.phone_no = phone_no;
        this.children_no =children_no;
    }

    public Parent(){
    }

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) { this.id = id; }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public int getChildren_no() { return children_no; }

    public void setChildren_no(int children_no) { this.children_no = children_no; }
}
