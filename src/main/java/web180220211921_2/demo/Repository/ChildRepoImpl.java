package web180220211921_2.demo.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import web180220211921_2.demo.Model.Child;

import java.util.List;

@Repository
public class ChildRepoImpl implements IChildRepo{
    @Autowired
    JdbcTemplate template;

    @Override
    public List<Child> fetchAll() {
        String sql="SELECT * FROM ddatabase.children";
        RowMapper<Child> rowMapper = new BeanPropertyRowMapper<>(Child.class);
        return template.query(sql,rowMapper);
    }

    @Override
    public List<Child> fetchActive() {
        String sql="SELECT * FROM ddatabase.children WHERE is_active = 1";
        RowMapper<Child> rowMapper = new BeanPropertyRowMapper<>(Child.class);
        return template.query(sql,rowMapper);
    }

    @Override
    public List<Child> fetchWaitingList() {
        String sql="SELECT * FROM ddatabase.children WHERE is_active = 0";
        RowMapper<Child> rowMapper = new BeanPropertyRowMapper<>(Child.class);
        return template.query(sql,rowMapper);
    }

    @Override
    public Child findChildById(int id) {
        String sql="SELECT * FROM ddatabase.children WHERE id=?";
        RowMapper<Child> rowMapper = new BeanPropertyRowMapper<>(Child.class);
        Child c=template.queryForObject(sql,rowMapper,id);
        return c;
    }

    @Override
    public Child addChild(Child c) {
        String sql="INSERT INTO ddatabase.children(parent_id, first_name, last_name, age, is_active) VALUES(?,?,?,?,?)";
        template.update(sql, c.getParentId(), c.getFirst_name(), c.getLast_name(), c.getAge(), c.getIs_active());
        return null;
    }

    @Override
    public Boolean deleteChild(int id) {
        String sql="delete from ddatabase.children where id=?";
        return template.update(sql,id)>=0;

    }

    @Override
    public Child updateChild(int id, Child c) {
        String sql="UPDATE ddatabase.children SET parent_id=?, first_name=?,last_name=?, age=? WHERE id=?";
        template.update(sql,c.getParentId(), c.getFirst_name(), c.getLast_name(), c.getAge(), id);
        return null;
    }

    @Override
    public Boolean acceptChild(int id) {
        String sql="UPDATE ddatabase.children SET is_active=1 WHERE id=?";
        return template.update(sql, id) >= 0;
    }
}
