package web180220211921_2.demo.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import web180220211921_2.demo.Model.Child;
import web180220211921_2.demo.Model.Employee;

import java.util.List;
@Repository
public class EmployeeRepoImpl implements IEmployeeRepo{
    @Autowired
    JdbcTemplate template;

    @Override
    public List<Employee> fetchAll() {
        String sql="SELECT * FROM ddatabase.employee";
        RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<>(Employee.class);
        return template.query(sql,rowMapper);
    }

    @Override
    public Employee findEmployeeById(int id) {
        String sql="SELECT * FROM ddatabase.employee WHERE id=?";
        RowMapper<Employee> rowMapper = new BeanPropertyRowMapper<>(Employee.class);
        Employee e=template.queryForObject(sql,rowMapper,id);
        return e;
    }

    @Override
    public Employee addEmployee(Employee e) {
        String sql="INSERT INTO ddatabase.employee(pin, first_name,last_name, address, phone_no, work_days) VALUES(?,?,?,?,?,?)";
        template.update(sql,e.getPin(), e.getFirst_name(), e.getLast_name(), e.getAddress(), e.getPhone_no(), e.getWork_days());
        return null;
    }

    @Override
    public Boolean deleteEmployee(int id) {
        String sql="delete from ddatabase.employee where id=?";
        return template.update(sql,id)>=0;
    }

    @Override
    public Employee updateEmployee(int id, Employee e) {
        String sql="UPDATE ddatabase.employee SET pin=?, first_name=?,last_name=?, address=?, phone_no=?, work_days=? WHERE id=?";
        template.update(sql,e.getPin(), e.getFirst_name(), e.getLast_name(), e.getAddress(), e.getPhone_no(), e.getWork_days(), id);
        return null;
    }
}
