package web180220211921_2.demo.Repository;

import web180220211921_2.demo.Model.Child;
import web180220211921_2.demo.Model.Employee;

import java.util.List;

public interface IEmployeeRepo {
    List<Employee> fetchAll();

    Employee findEmployeeById(int id);

    Employee addEmployee(Employee e);

    Boolean deleteEmployee(int id);

    Employee updateEmployee(int id, Employee e);
}
