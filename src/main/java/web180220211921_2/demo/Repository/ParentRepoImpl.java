package web180220211921_2.demo.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import web180220211921_2.demo.Model.Child;
import web180220211921_2.demo.Model.Employee;
import web180220211921_2.demo.Model.Parent;

import java.util.List;

@Repository
public class ParentRepoImpl implements IParentRepo{
    @Autowired
    JdbcTemplate template;
    @Override
    public List<Parent> fetchAll() {
        String sql="SELECT * FROM ddatabase.parents";
        RowMapper<Parent> rowMapper = new BeanPropertyRowMapper<>(Parent.class);
        return template.query(sql,rowMapper);
    }

    @Override
    public Parent findParentById(int id) {
        String sql="SELECT * FROM ddatabase.parents WHERE id=?";
        RowMapper<Parent> rowMapper = new BeanPropertyRowMapper<>(Parent.class);
        Parent p=template.queryForObject(sql,rowMapper,id);
        return p;
    }

    @Override
    public Parent addParent(Parent p) {
        String sql="INSERT INTO ddatabase.parents(first_name, last_name, address, phone_no, children_no) VALUES(?,?,?,?,?)";
        template.update(sql,p.getFirst_name(), p.getLast_name(), p.getAddress(), p.getPhone_no(), p.getChildren_no());
        return null;
    }

    @Override
    public Boolean deleteParent(int id) {
        String sql="delete from ddatabase.parents where id=?";
        return template.update(sql,id)>=0;
    }

    @Override
    public Parent updateParent(int id, Parent p) {
        String sql="UPDATE ddatabase.parents SET first_name=?, last_name=?, address=?, phone_no=?, children_no=? WHERE id=?";
        template.update(sql,p.getFirst_name(), p.getLast_name(), p.getAddress(), p.getPhone_no(), p.getChildren_no(), id);
        return null;
    }
}
