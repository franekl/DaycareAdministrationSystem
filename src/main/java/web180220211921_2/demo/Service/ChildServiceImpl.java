package web180220211921_2.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web180220211921_2.demo.Model.Child;
import web180220211921_2.demo.Repository.IChildRepo;
import java.util.List;

@Service
public class ChildServiceImpl implements IChildService{
    @Autowired
    IChildRepo childRepo;
    @Override
    public List<Child> fetchAll() {
        return childRepo.fetchAll();
    }

    @Override
    public List<Child> fetchActive() {
        return childRepo.fetchActive();
    }

    @Override
    public List<Child> fetchWaitingList() {
        return childRepo.fetchWaitingList();
    }

    @Override
    public Child findChildById(int id) {
        return childRepo.findChildById(id);
    }

    @Override
    public Child addChild(Child c) {
        return childRepo.addChild(c);
    }

    @Override
    public Boolean deleteChild(int id) {
        return childRepo.deleteChild(id);
    }

    @Override
    public Child updateChild(int id, Child c) {
        return childRepo.updateChild(id, c);
    }

    @Override
    public Boolean acceptChild(int id) {
        return childRepo.acceptChild(id);
    }

}
