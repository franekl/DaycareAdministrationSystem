package web180220211921_2.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web180220211921_2.demo.Model.Employee;
import web180220211921_2.demo.Repository.IChildRepo;
import web180220211921_2.demo.Repository.IEmployeeRepo;

import java.util.List;

@Service
public class EmployeeServiceImpl implements IEmployeeService{
    @Autowired
    IEmployeeRepo employeeRepo;

    @Override
    public List<Employee> fetchAll() {
        return employeeRepo.fetchAll();
    }

    @Override
    public Employee findEmployeeById(int id) {
        return employeeRepo.findEmployeeById(id);
    }

    @Override
    public Employee addEmployee(Employee e) {
        return employeeRepo.addEmployee(e);
    }

    @Override
    public Boolean deleteEmployee(int id) {
        return employeeRepo.deleteEmployee(id);
    }

    @Override
    public Employee updateEmployee(int id, Employee e) {
        return employeeRepo.updateEmployee(id, e);
    }

    @Override
    public boolean signin(int id, String pin) {
        if(employeeRepo.findEmployeeById(id).getPin().equals(pin)){
            return true;
        }
        else
            return false;
    }
}
