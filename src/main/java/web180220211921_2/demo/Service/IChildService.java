package web180220211921_2.demo.Service;

import web180220211921_2.demo.Model.Child;
import java.util.List;

public interface IChildService {
    List<Child> fetchAll();

    List<Child> fetchActive();

    List<Child> fetchWaitingList();

    Child findChildById(int id);

    Child addChild(Child c);

    Boolean deleteChild(int id);

    Child updateChild(int id, Child c);

    Boolean acceptChild(int id);
}
