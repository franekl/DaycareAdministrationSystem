package web180220211921_2.demo.Service;

import web180220211921_2.demo.Model.Employee;

import java.util.List;

public interface IEmployeeService {
    List<Employee> fetchAll();

    Employee findEmployeeById(int id);

    Employee addEmployee(Employee е);

    Boolean deleteEmployee(int id);

    Employee updateEmployee(int id, Employee е);

    boolean signin(int id, String pin);
}
