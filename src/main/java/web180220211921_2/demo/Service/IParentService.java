package web180220211921_2.demo.Service;

import web180220211921_2.demo.Model.Parent;

import java.util.List;

public interface IParentService {
    List<Parent> fetchAll();

    Parent findParentById(int id);

    Parent addParent(Parent p);

    Boolean deleteParent(int id);

    Parent updateParent(int id, Parent p);
}
