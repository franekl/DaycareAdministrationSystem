package web180220211921_2.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web180220211921_2.demo.Model.Parent;
import web180220211921_2.demo.Repository.IEmployeeRepo;
import web180220211921_2.demo.Repository.IParentRepo;

import java.util.List;

@Service
public class ParentServiceImpl implements IParentService{
    @Autowired
    IParentRepo parentRepo;

    @Override
    public List<Parent> fetchAll() {
        return parentRepo.fetchAll();
    }

    @Override
    public Parent findParentById(int id) {
        return parentRepo.findParentById(id);
    }

    @Override
    public Parent addParent(Parent p) {
        return parentRepo.addParent(p);
    }

    @Override
    public Boolean deleteParent(int id) {
        return parentRepo.deleteParent(id);
    }

    @Override
    public Parent updateParent(int id, Parent p) {
        return  parentRepo.updateParent(id, p);
    }
}
